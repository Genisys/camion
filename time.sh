#!/bin/bash
#set -xv

### Variables ###
decimal=0
mitnute=0
heure=0
seconde=0
nbpause=0
distance=0

echo -e "De quel ville voulez vous partir ?"; read from
echo -e "Vers ou voulez vous aller ?"; read vers
echo "Cela peut prendre prendre un peu de temps veuillez patienter s'il vous plait"

### Récupere les donnees de distance ###
total=`wget -q -O - "https://www.bonnesroutes.com/distance/?from=$from&to=$vers" |grep -i -A1 "total_distance" |tr -d [:punct:][:alpha:][:space:]`
total=`echo $total |sed -E 's/(\xc2|\xa0)*//g'`
distance=$total

# Je rajoute cette commande car l espace pour les gros killometrage fait tout claquer
# distance=`echo $distance |tr -d [:space:]`
# distance=`echo $distance |sed 's/ //'`
# distance=`echo $distance |awk '{ gsub (" ", "", $0); print}'`

### Calcul du Temps ###
#decimal=`echo "scale=2; $distance / 90" |bc`
decimal=`echo "scale=2; $total / 90" |bc`
seconde=`echo "$decimal * 3600" |bc |awk -F"." '{print $1}'`

#if [ $seconde -ge 7200 ] ; then
#    nbpause=`expr $seconde / 7200`
#    seconde=`expr \( $nbpause \* 900 \) + $seconde`
#else
#   :
#fi

# Pour les pauses
distance=`echo "scale=2; $distance - 7.5" |bc |awk -F"." '{print $1}'`
seconde=`expr $seconde + 540`
pause_seconde=$seconde

while [ $distance -gt 0 ]
    do 
        if [ $pause_seconde -gt 7200 ] ; then
            ((nbpause+=1))
            seconde=`expr $seconde + 900`
            pause_seconde=`expr $pause_seconde - 7200  + 900`
            distance=`echo "scale=2; ($distance - 15 - 166.5)" |bc |awk -F"." '{print $1}'`
        elif [ $pause_seconde -le 7200 ] ; then
            pause_seconde=`expr ((($distance - 7.5) / 90) * 3600)"|bc`
            seconde=`expr $seconde + $pause_seconde + 540`
            distance=0
        else
            :
        fi
    done

# Pour le temps du trajet
if [ $(expr $seconde / 60) -ge 0 ] || [ $(echo "scale=2; $seconde / 3600" |bc) -ge 1 ] ; then
    if [ $(expr $seconde / 3600) -ge 0 ] || [ $(echo "scale=2; $seconde / 3600" |bc) -ge 1 ] ; then
        heure=`expr $seconde / 3600`
        minute=`expr \( $seconde % 3600 \) / 60`
    else 
        minute=`expr $seconde / 60`
    fi
else 
        minute=`expr $seconde / 60`
fi

### Resultat ###
echo "-------- Carnet de bord ---------"
echo "Trajet : $from vers $vers"
if [ $heure -eq 0 ] ; then
    echo "Temps de trajet : $minute minutes"
else
    echo "Temps de trajet : $heure:$minute heures"
fi
echo "Nombre de Pause : $nbpause"
echo "Temps de Pause : $(expr $nbpause \* 15) minutes"
echo "Distance Parcouru : $total Killomètres "
echo "---------------------------------"

